## Project Management
Project Management

Week 1 Project Management
Gitlab account geregistreert vervolgens

Github accounts registreren

Toegang tot eigen Repositories met Github desktop naar mijn lokale computer gekloond 

Kennis gemaakt met Basic markdown syntax

Wijzigingen aangebracht in mijn repository waaronder het toevoegen van basisteksten en afbeeldingen 

Daarna met behulp van  Github desktop sync veranderingen gebracht bij de hosted  repository bij Gitlab

Schrijven over ons zelf

Brainstormen over eindproduct en eventueel onderzoeken online

Basic Documentation outline

Alle stappen documenteren door middel van Screenshots

1.Cloning from Gitlab

![MEDAVENI](../img/1.png)

2.Clone a repository

![MEDAVENI](../img/2.png)

3.Local File System

![MEDAVENI](../img/3.png)

4.Change made in Notepad ++

![MEDAVENI](../img/4.png)

5.Github Desktop auto detection + commit text

![MEDAVENI](../img/5.png)

6.Pushing to Gitlab

![MEDAVENI](../img/6.png)

7.Gitlab Change Made. 

![MEDAVENI](../img/7.png)