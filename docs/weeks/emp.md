int lightInt= 0;
//int analogPin= A0;
int minLight=800;
int maxLight=1023;
int lightPc=0;                          // the setup routine runs once when you press reset
//int sensorValue= 0;

```
void setup() {                           //void (omdat je er niet meer te maken heb)
       Serial.println("Party");
       Serial.begin(9600);  
       pinMode(3,OUTPUT);
       pinMode(4,OUTPUT);
       pinMode(5,OUTPUT); 
}                                       // initialize serial communication at 9600 bits per second
                                        //beginnen een serial connectie met een 9600 bits per sec
void LedsOff(){
    digitalWrite(3,LOW);
    digitalWrite(4,0);
    digitalWrite(5,0);
}

void LedsRunning(){
    LedsOff();
    digitalWrite(3,1);
    delay(250);
    LedsOff();
    digitalWrite(4,1);
    delay(250);
    LedsOff();
    digitalWrite(5,1);
    delay(250);                                   // the loop routine runs over and over again forever:
}

void loop() {
    lightInt = analogRead(A0); 
    lightPc = (lightInt-minLight)*100L/(maxLight-minLight); // read the input on analog pin 0
    Serial.println(lightPc); 
    LedsOff();
      if(lightPc >=30){digitalWrite(3,1);};
      if(lightPc >=60){digitalWrite(4,1);};
      if(lightPc >=90){digitalWrite(5,1);};
      if(lightPc >=95){LedsRunning();};                       
      delay(1000);
}
```

![Tinkercad 1](.../img/Tinkercad 1.png)

```

int lightInt = 0;
int lightPc = 0;
int minimumlight = 400;
int maximumlight = 1023;

void setup() {
  pinMode (3,OUTPUT);
  pinMode (4,OUTPUT);
  pinMode (5,OUTPUT);
  Serial.begin(9600);
  Serial.println ("helllo");

}
void ledsOff() {
//switch all leds off
digitalWrite(3,0);
digitalWrite(4,0);
digitalWrite(5,0);
}

void runningLeds(){
  ledsOff();
  digitalWrite(3,1);
  delay(250);
  ledsOff();
  digitalWrite(4,1);
  delay(250);
  ledsOff();
  digitalWrite(5,1);
  delay(250);
}


void loop() {
  lightInt = analogRead (A0);
  //Serial.println (lightInt);
  //Measure and determin min & max values observed 
  lightPc = (lightInt-minimumlight)*100L/(maximumlight - minimumlight) ;
  Serial.println (lightPc);
  ledsOff ();
  // if light greater and equal to 30% led 1 on 
  // if light greater and equal to 60% led 2 on 
  // if light greater and equal to 90% led 3 on 
 if (lightPc >= 30 ){digitalWrite(3,1);};
 if (lightPc >= 60 ){digitalWrite(4,1);};
 if (lightPc >= 90 ){digitalWrite(5,1);};
 if (lightPc > 95 ){runningLeds();};
 delay (1000); 

}

```
![Lightintensity](.../img/Lightintensity.png)