- Week 2 
- Video Production
- We learned how to install Adobe Premiere
- The steps of Video editing

- How to open Adobe Premiere after installing

![tag](../img/vp/open.png)

- How to create a new Project

![tag](../img/vp/new.png)

- How to give your project a new name.
-Capture Format select HDV

![tag](../img/vp/project.png)

- Import file

![tag](../img/vp/import.png)

- How to drag video to the timeline right
- The workspace

![tag](../img/vp/drag.png)

- How to select effects

![tag](../img/vp/effects.png)

- How to search for the Ultra Key
- Drag it right on video

![tag](../img/vp/ultra.png)

- The effect control
 
![tag](../img/vp/control.png)

- How to change the key color 

![tag](../img/vp/keycolor.png)

- How to remove the current background

![tag](../img/vp/background.png)

- How to import ( Ctrl+I) a background picture
- Drag to right between video and audio
- If it small, you can make it biiger with "scale"

![tag](../img/vp/import.png)

- How to add Title

![tag](../img/vp/legacytitle.png)

- How t give it a new name

![tag](../img/vp/name.png)

- How to type the Title
- Select any Text Font
- Select any color

![tag](../img/vp/title.png)

- How to drag the title right between video background
- Stretch as pleased

![tag](../img/vp/drag.png)

- How to export the file

![tag](../img/vp/fileexport.png)

- Export Settings
- Format H.264

![tag](../img/vp/exportsettings.png)

- Select "Preset"
- Youtube 720p HD

![tag](../img/vp/youtube.png)

- Output Name

![tag](../img/vp/output.png)

- Final Export

![tag](../img/vp/finalexport.png)
